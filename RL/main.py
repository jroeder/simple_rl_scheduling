import torch
import time
from torch.utils.data import DataLoader
import numpy as np

from utils import *
from Data import Data
from Agent import Agent
import config

import wandb

DEBUG = True

def main():
    # parse yaml config file
    cfg, _ = config.parse_args("Train Scheduler Testing")

    if not __debug__:
        # init W&B
        wandb.init(project="rl_scheduling")

        # getting all the config info th W&B
        wconfig = wandb.config  # Initialize config
        wconfig.batch_size = cfg.batch_size  # input batch size
        wconfig.buffer_size = cfg.buffer_size  # buffer for memory
        wconfig.min_replay_memory_size = cfg.min_replay_memory_size # minimum number of elements in replay mem
        wconfig.epochs = cfg.epochs  # number of epochs to train (default: 10)
        wconfig.lr = cfg.lr  # learning rate (default: 0.01)
        wconfig.gamma = cfg.gamma # value of gamma - discounts
        wconfig.tau = cfg.tau # impact of soft update on target network
        wconfig.update_target_frequency = cfg.update_target_frequency # how often the target is updated
        wconfig.start_eps = cfg.start_eps # the initial starting value of epsilon (how likely is it that we take a random action)
        wconfig.eps_decay = cfg.eps_decay # how much does this decay
        wconfig.seed = cfg.seed  # random seed (default: 42)
        wconfig.log_interval = cfg.logging_frequency  # how many batches to wait before logging training status - not used
        wconfig.optimizer = cfg.optimizer
        wconfig.loss = cfg.loss

    # load the training dataset
    dataset_train = Data(cfg.training_data, 4, cfg.training_data_struct2vec_path)
    dataloader_train = DataLoader(dataset_train, batch_size=1, shuffle=False, collate_fn=collate_fn)

    # load the test dataset
    dataset_test = Data(cfg.test_data, 4, cfg.test_data_struct2vec_path)
    dataset_test.set_opt_graph(cfg.test_data_opt)
    dataloader_test = DataLoader(dataset_test, batch_size=1, shuffle=False, collate_fn=collate_fn)

    # SET SEEDS
    # torch.manual_seed(cfg.seed)
    # torch.cuda.manual_seed_all(cfg.seed)
    seed_all(cfg.seed)

    # get number of featuresz
    possible_tasks = dataset_train[0].get_list_actions()
    states = dataset_train[0].get_list_of_features(possible_tasks[0])
    print(states)
    # init the agent
    agent = Agent(len(states), 4, cfg)

    if not __debug__:
        wandb.watch(agent.qnetwork_target)

    # trainingW
    for epoch in range(cfg.epochs):
        start = time.time()
        error, average_test_makespan, ratio_RL_better, makespan_fls_test = test(dataloader_test, agent, epoch)
        print(time.time() - start)

        makespan_sum = train(dataloader_train, agent)

        # makespan_sum = 0
        if not __debug__:
            wandb.log({"Test Mean Error": error, "Test Mean Makespan":  average_test_makespan, "Train Makespan Sum": makespan_sum, "Test % RL better": ratio_RL_better})

if __name__ == "__main__":
    main()