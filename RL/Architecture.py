class Architecture():
    """
    Architecture model that we work with.
    For now it only inccludes some information regarding the possible start time of a task.
    It takes into account the start time of predecessors.
    """

    def __init__(self, num_cores=None):
        self.cores_availability = {}
        self.core_avail_next = {}
        self.core_utilisation_time = {}
        self.num_cores = num_cores
        if num_cores:
            self.init_cores()

    def init_cores(self):
        for i in range(self.num_cores):
            self.core_avail_next['core' + str(i)] = 0
            self.cores_availability['core' + str(i)] = []
            self.core_utilisation_time['core' + str(i)] = 0

    def increase_core_time(self, core, start, runtime):
        """
        Because of start and runtime we know when a core should start and when its done.
        :param core: which cores time needs to be increased
        :param start: when the task should start
        :param runtime: how long the task takes
        :return:nothing
        """
        if self.core_avail_next[core] == start:
            self.core_avail_next[core] += runtime
        else:
            diff = start - self.core_avail_next[core]
            self.core_avail_next[core] += diff
            self.core_avail_next[core] += runtime

        self.cores_availability[core].append([start, start + runtime])
        self.core_utilisation_time[core] += runtime

    def get_cores_availability(self):
        return self.core_avail_next

    def get_task_best_time(self, min_start_time, runtime, core):
        for time in self.cores_availability[core]:
            prev_start_time = time[0]
            prev_end_time = time[1]

            if prev_start_time < (min_start_time + runtime) and min_start_time < prev_end_time:
                min_start_time = prev_end_time

        return min_start_time

    def get_max_gap_all_cores(self):
        """
        :return: list of size num_cores with the largest gap between items for each core
        """
        max_gaps = []
        max_gaps_start = []
        average_gaps = []
        num_gaps = []
        for core_times in self.cores_availability.items():
            last_end_time = 0
            max_gap = 0
            gap = 0
            average_gap = 0
            num_gap = 0
            max_gap_start = 0
            for time in core_times[1]:
                start_time = time[0]
                end_time = time[1]
                if start_time > last_end_time:
                    gap = start_time - last_end_time
                    if gap > max_gap:
                        max_gap = gap
                        max_gap_start = start_time
                    if gap > 0:
                        average_gap += gap
                        num_gap += 1
                last_end_time = end_time
            max_gaps.append(max_gap)
            max_gaps_start.append(max_gap_start)
            if num_gap > 0:
                average_gaps.append(average_gap/num_gap)
            else:
                average_gaps.append(0)
            num_gaps.append(num_gap)

        while len(max_gaps) < self.num_cores:
            max_gaps.append(0)

        return max_gaps + max_gaps_start + average_gaps + num_gaps

    def get_core_utilisation(self, makespan):
        """
        :param makespan: the current
        :return:
        """
        util = []
        for c in self.core_utilisation_time.items():
            if(makespan == 0):
                util.append(0)
            else:
                util.append(c[1]/makespan)

        return util

    def get_cluster_utilisation(self):
        # TODO cluster utilization - no clusters yet
        pass