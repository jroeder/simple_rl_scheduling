import re
import numpy as np
import torch
import random
import torch.backends.cudnn
import time

constant_priority = 0.1
DEBUG = False

def get_num(s):
    """
    :param s: some input string
    :return: a list of all floats found
    """
    l = []
    for t in s.split():
        try:
            l.append(float(t))
        except ValueError:
            pass
    if not l:
        l = [re.findall(r"[-+]?\d*\.\d+|\d+", s)]
        # print(l)
    return l


def train(dataloader, agent):
    makespan_sum = []
    # for i in range(500):
    for env in dataloader:
        score = train_dqn(env, agent, 0)
        makespan_sum.append(env.schedule.get_makespan())
        env.reset()

    return np.mean(makespan_sum)

def test(dataloader, agent, epoch):
    temp_scores = []
    makespan = []
    makespan_fls = []
    num_tasks = []
    time_list = []

    RL_better = 0
    for env in dataloader:
        env.reset()
        start = time.time()
        score = test_dqn(env, agent)
        time_list.append(time.time() - start)

        if env.task_graph.ILP_makespan > 0:
            temp_scores.append((env.schedule.get_makespan() - env.task_graph.ILP_makespan)) #
            if (temp_scores[-1] < 0):
                RL_better += 1
        makespan.append(env.schedule.get_makespan())
        makespan_fls.append(env.task_graph.ILP_makespan)
        num_tasks.append(len(env.task_graph.tasks))

        if DEBUG:
            env.schedule.draw_schedule(epoch)

    file = "../out/" + str(epoch) + "makespan_results.csv"
    with open(file, "w") as f:
        for make in zip(makespan, makespan_fls, num_tasks, time_list):
            f.write("Num Tasks: " + str(make[2]) + " RL makespan: " + str(make[0]) + " FLS makespan: " + str(make[1]) + " Time taken: " + str(make[3]) + "\n")


    return np.mean(temp_scores), np.mean(makespan), (RL_better/len(temp_scores)), np.mean(makespan_fls)

# TODO: make sure "dones" are used properly
def train_dqn(env, agent, score):
    """
    Training the agent
    """
    # make list of all available tasks and their features
    states = {}
    possible_tasks = env.get_list_actions()
    if not possible_tasks:
        return score

    for pt in possible_tasks:
        states[pt] = env.get_list_of_features(pt)

    action = agent.schedule_next_tasks_train(states)
    reward, next_states = env.step(action)
    global_reward = env.schedule.calculate_global_reward()
    if next_states:
        done = False
    else:
        done = True
    agent.step(states[action[0]], action[1], reward, next_states, done, global_reward, action[2])

    score += reward
    return (train_dqn(env, agent, score))


def test_dqn(tg, agent):
    """
    Testing the agent results
    """
    # make list of all available tasks and their features
    states = {}
    possible_tasks = tg.get_list_actions()
    if not possible_tasks:
        tg.schedule.calculate_makespan()
        return tg.schedule.calculate_global_reward()

    for pt in possible_tasks:
        states[pt] = tg.get_list_of_features(pt)

    action = agent.schedule_next_tasks_test(states)
    reward, next_states = tg.step(action)

    return (test_dqn(tg, agent))


def collate_fn(batch):
    """
    For the pytorch dataloader
    """
    return batch[0]


def seed_all(seed):
    # https://discuss.pytorch.org/t/reproducibility-with-all-the-bells-and-whistles/81097

    if not seed:
        seed = 10

    print("[ Using Seed : ", seed, " ]")

    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.cuda.manual_seed(seed)
    np.random.seed(seed)
    random.seed(seed)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
