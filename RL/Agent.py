import torch
import torch.optim as optim
from collections import namedtuple, deque
import numpy as np
import random

from RL_Network import QNetwork

class Agent():
    """
    The agent that "learns" or trains the neural networks based on actions in the environment and the impact.
    """

    def __init__(self, state_size, action_size, cfg):

        self.tau = cfg.tau
        self.min_replay_memory_size = cfg.min_replay_memory_size
        self.update_target_frequency = cfg.update_target_frequency
        self.eps_decay = cfg.eps_decay
        self.eps = cfg.start_eps
        self.batch_size = cfg.batch_size
        self.gamma = cfg.gamma
        self.lr = cfg.lr
        self.buffer_size = cfg.buffer_size
        self.device = torch.device(cfg.device)
        self.loss = cfg.loss
        self.PRB = cfg.PRB

        assert (self.buffer_size >= self.min_replay_memory_size), "Buffer must be larger than the minimum replay size"
        assert (self.buffer_size >= self.batch_size), "Buffer must be larger than the batch size"


        self.state_size = state_size
        self.action_size = action_size
        #         self.device = torch.device("cpu")

        # Q- Network
        self.qnetwork_local = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 128, 64, 32).to(self.device)
        self.qnetwork_target = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 128, 64, 32).to(self.device)
        # self.qnetwork_local = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 512).to(self.device)
        # self.qnetwork_target = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 512).to(self.device)
        # self.qnetwork_local = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 256, 512, 512).to(self.device)
        # self.qnetwork_target = QNetwork(state_size, action_size, cfg.dropout, 64, 128, 256, 256, 512, 512).to(self.device)

        ## set weights of networks the same??
        for target_param, local_param in zip(self.qnetwork_target.parameters(), self.qnetwork_local.parameters()):
            target_param.data.copy_(local_param.data)

        if cfg.optimizer == "Adam":
            self.optimizer = optim.Adam(self.qnetwork_local.parameters(), lr=self.lr)
            # self.optimizer = optim.Adam(self.qnetwork_local.parameters(), lr=self.lr, amsgrad=True)
        elif cfg.optimizer == "Adadelta":
            self.optimizer = optim.Adadelta(self.qnetwork_local.parameters())
        elif cfg.optimizer == "Adagrad":
            self.optimizer = optim.Adagrad(self.qnetwork_local.parameters(), lr=self.lr, lr_decay=0.99)
        elif cfg.optimizer == "SGD":
            self.optimizer = optim.SGD(self.qnetwork_local.parameters(), lr=self.lr, momentum=0.5)
        elif cfg.optimizer == "RMSprop":
            self.optimizer = optim.RMSprop(self.qnetwork_local.parameters(), lr=self.lr)
        elif cfg.optimizer == "ASGD":
            self.optimizer = optim.ASGD(self.qnetwork_local.parameters(), lr=self.lr)
        else:
            ValueError("Unknown Optimizer")

        # self.lr_scheduler = torch.optim.lr_scheduler.CyclicLR(self.optimizer, self.lr/2, self.lr*2, cycle_momentum=False)


        # Replay memory
        if cfg.PRB:
            self.memory = PriorityReplayBuffer(self.buffer_size, self.batch_size)
            if (self.buffer_size != self.min_replay_memory_size):
                raise UserWarning("buffer size and min replay memory size have to be the same for Priority replay buffers")
        else:
            self.memory = ReplayBuffer(self.buffer_size, self.batch_size)
        # Initialize time step (for updating every UPDATE_EVERY steps)
        self.t_step = 0

    def schedule_next_tasks_train(self, states):
        """This determines the state and action picked for a larger set of possible states"""
        # get state features as tensor
        state_features = torch.FloatTensor(list(states.values())).float()
        state_features = state_features.to(self.device)

        # sets network to eval mode
        self.qnetwork_local.eval()
        # predict the best actions for all the different states
        with torch.no_grad():
            prediction = self.qnetwork_local(state_features).detach().cpu()
            highest_values_next, next_selected_actions = prediction.max(1, True)

        # figure out which one of the states actually results in the highest reward.
        # i.e. which task should be scheduled next and what action should that task ahve
        highest_values_next, next_selected_task_num = highest_values_next.max(0)
        next_selected_actions = int(next_selected_actions[next_selected_task_num])
        next_selected_task = list(states.keys())[next_selected_task_num]

        if self.eps < 0.05:
            print("reset eps")
            self.eps = 0.99

        # sets network to train mode
        self.qnetwork_local.train()
        # either pass back the "best choice" or a random choice
        if random.random() > self.eps:
            return [next_selected_task, next_selected_actions, highest_values_next]
        else:
            # completely random next task and action
            random_task_num = random.randrange(len(states.keys()))
            random_task = list(states.keys())[random_task_num]
            random_action = random.choice(np.arange(self.action_size))
            predicted_value = prediction[random_task_num, random_action]
            return [random_task, random_action, predicted_value]

    def schedule_next_tasks_test(self, states):
        """This determines the state and action picked for a larger set of possible states"""
        # get state features as tensor
        state_features = torch.FloatTensor(list(states.values())).float()
        state_features = state_features.to(self.device)

        # sets network to eval mode
        self.qnetwork_target.eval()
        # predict the best actions for all the different states
        with torch.no_grad():
            highest_values_next, next_selected_actions = self.qnetwork_target(state_features).detach().max(1, True)

        # figure out which one of the states actually results in the highest reward.
        # i.e. which task should be scheduled next and what action should that task ahve
        highest_values_next, next_selected_task_num = highest_values_next.max(0)
        next_selected_actions = int(next_selected_actions[next_selected_task_num])
        next_selected_task = list(states.keys())[next_selected_task_num]

        # either pass back the "best choice" or a random choice
        return [next_selected_task, next_selected_actions]

    def step(self, state, action, reward, next_step, done, global_reward, priority):
        """this adds info to the replay buffer and takes care of training at the right time"""
        # Save experience in replay memory
        self.memory.add(state, action, reward, next_step, done, global_reward, priority)
        if len(self.memory) < self.min_replay_memory_size:
            return

        # Learn every UPDATE_EVERY time steps.
        self.t_step = (self.t_step + 1) % self.update_target_frequency
        if self.t_step == 0:
            # If enough samples are available in memory, get radom subset and learn
            self.eps *= self.eps_decay

            if len(self.memory) > self.batch_size:
                if self.PRB:
                    tree_idx, experience, weights = self.memory.sample(self.device)
                    self.learn(experience, tree_idx, weights)
                else:
                    experience = self.memory.sample(self.device)
                    self.learn(experience)

    def learn(self, experiences, tree_idx=None, IS_weights=None):
        """
        This trains the local network. The local network is trained on a given batchsize at certain timesteps.
        This prevents the local network from going all over the place.
        The update between local and target network is even slower.
        """
        states, actions, rewards, next_states, dones, global_reward = experiences

        if self.loss == "MSELoss":
            criterion = torch.nn.MSELoss(reduction='none')
        elif self.loss == "L1Loss":
            criterion = torch.nn.L1Loss(reduction='none')
        elif self.loss == "SmoothL1Loss":
            criterion = torch.nn.SmoothL1Loss(reduction='none')
        elif self.loss == "HingeEmbeddingLoss":
            criterion = torch.nn.HingeEmbeddingLoss()
        elif self.loss == "CosineEmbeddingLoss":
            criterion = torch.nn.CosineEmbeddingLoss()
        elif self.loss == "KLDivLoss":
            criterion = torch.nn.KLDivLoss()
        else:
            ValueError("Unknown Optimizer")

        self.qnetwork_local.train()
        self.qnetwork_target.eval()

        # predict the reward for the action pickeed for each state
        # fixed
        predicted_rewards = self.qnetwork_local(states).gather(1, actions)

        # for each state we have multiple possible next states and need to compute them all
        # the predictions of the next state happens in batches
        future_rewards = []
        for i, next_state in enumerate(next_states):
            next_state_features = torch.tensor(list(next_state.values())).to(self.device)
            if not next_state:
                # if there are no more next states (i.e. at the end we just get the global reward)
                future_rewards.append(float(global_reward[i]))
            else:
                # predicts which task would be scheduled next and what action would be taken.
                # next_selected_task is the highest return for all the possible tasks.
                # next_selected_action is the core to which the task would be scheduled to
                with torch.no_grad():
                    actions_q_local = self.qnetwork_local(next_state_features).detach().max(1)[1].unsqueeze(1).long()
                    labels_next = self.qnetwork_target(next_state_features).gather(1, actions_q_local)

                highest_reward, next_selected_task_num = labels_next.max(0)

                # I think we dont want the action but the reward of these best future actions
                future_rewards.append((self.gamma * highest_reward))

        # calculate the "future_rewards" for all the states in the batch
        future_rewards = torch.tensor(future_rewards).unsqueeze(1).to(self.device)
        future_rewards = (rewards + (future_rewards * (1 - dones)))

        if self.PRB:
            abs_errors = abs((predicted_rewards - future_rewards)/future_rewards)
            self.memory.batch_update(tree_idx, abs_errors.detach().cpu())

        # backpropagate the local network
        loss = criterion(predicted_rewards, future_rewards).to(self.device)

        if self.PRB:
            loss *= IS_weights
        loss = torch.mean(loss)
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
        # self.lr_scheduler.step()

        # ------------------- update target network ------------------- #
        update_target = self.t_step % (self.update_target_frequency * self.tau)
        if update_target == 0:
            self.soft_update(self.qnetwork_local, self.qnetwork_target, self.tau)

    def soft_update(self, local_model, target_model, tau):
        """Soft update model parameters.
        θ_target = τ*θ_local + (1 - τ)*θ_target
        Params
        =======
            local model (PyTorch model): weights will be copied from
            target model (PyTorch model): weights will be copied to
            tau (float): interpolation parameter
        """
        for target_param, local_param in zip(target_model.parameters(), local_model.parameters()):
            # target_param.data.copy_(tau * local_param.data + (1 - tau) * target_param.data)
            target_param.data.copy_(local_param.data)

class PriorityReplayBuffer:
    """
    Fixed size buffer implemented with a sum tree for easy priority selection and updating
    This SumTree code is modified version and the original code is from:
    https://gist.github.com/simoninithomas/d6adc6edb0a7f37d6323a5e3d2ab72ec
    """
    data_pointer = 0
    num_data_points = 0

    PER_e = 0.01  # Hyperparameter that we use to avoid some experiences to have 0 probability of being taken
    PER_a = 0.6  # Hyperparameter that we use to make a tradeoff between taking only exp with high priority and sampling randomly
    PER_b = 0.4  # importance-sampling, from initial value increasing to 1
    PER_b_increment_per_sampling = 0.001
    absolute_error_upper = 1.  # clipped abs error

    def __init__(self, buffer_size, batch_size):
        """
        :param action_size:
        :param buffer_size:
        :param batch_size:
        """
        self.buffer_size = buffer_size

        self.tree = np.zeros(2 * buffer_size - 1) # size of tree
        self.data = np.zeros(buffer_size, dtype=object)

        self.batch_size = batch_size
        self.experiences = namedtuple("Experience", field_names=["state",
                                                                 "action",
                                                                 "reward",
                                                                 "next_state",
                                                                 "done",
                                                                 "global_reward"])

    def add(self, state, action, reward, next_state, done, global_reward, predicted_reward):
        """
        add info to the tree and update the data
        """

        # where in the tree we want to put stuff
        tree_index = self.data_pointer + self.buffer_size - 1

        # update data
        e = self.experiences(state, action, reward, next_state, done, global_reward)
        self.data[self.data_pointer] = e

        # calculate priority etc
        # abs_error = abs(predicted_reward - reward)
        abs_error = abs((predicted_reward - reward)/reward)

        # update tree
        self.update(tree_index, abs_error)

        # update data_pointer
        self.data_pointer += 1
        if (self.data_pointer >= self.buffer_size):
            self.data_pointer = 0

        # keep track of buffer size
        if (self.num_data_points < self.buffer_size):
            self.num_data_points +=1

    def batch_update(self, tree_idx, abs_errors):
        for ti, p in zip(tree_idx, abs_errors):
            self.update(ti, p)

    def update(self, tree_index, abs_error):
        """
        update leaf priority score and propagate the changes
        :param tree_index:
        :param priority:
        :return:
        """
        # calculate priority
        abs_error += self.PER_e
        clipped_errors = np.minimum(abs_error, self.absolute_error_upper)
        priority = np.power(clipped_errors, self.PER_a)

        # calculate the change and update the leaf
        priority_change = priority - self.tree[tree_index]
        self.tree[tree_index] = priority

        # propagate change through tree
        while tree_index != 0:
            tree_index = (tree_index - 1) // 2
            self.tree[tree_index] += priority_change

    def get_leaf(self, value):
        """
        Search the tree for the value given.
        :param value: the value/ priority we are looking for
        :return: index of the leaf, the value at that leaf and the experience for that leaf
        """
        parent_index = 0

        while True:
            left_child_index = 2 * parent_index + 1
            right_child_index = left_child_index + 1

            if left_child_index >= len(self.tree):
                leaf_index = parent_index
                break
            else:
                if (value <= self.tree[left_child_index]):
                    parent_index = left_child_index
                else:
                    value -= self.tree[left_child_index]
                    parent_index = right_child_index

        data_index = leaf_index - self.buffer_size + 1

        return leaf_index, self.tree[leaf_index], self.data[data_index]

    def make_batch(self):
        """
        :return:
        """
        # contains the actual experience data
        mini_batch = []

        # contains the original index and the experience IS weights
        experience_idx , experience_ISWeights = np.empty((self.batch_size,), dtype=np.int32), np.empty((self.batch_size, 1), dtype=np.float32)

        # needed for equally sized segments
        priority_segment = self.total_priority / self.batch_size

        # update the PER_b hyperparameter
        self.PER_b = np.min([1, self.PER_b + self.PER_b_increment_per_sampling])

        # get the Probability of picking the experience with the lowest priority
        p_min = np.min(self.tree[-self.buffer_size:]) / self.total_priority
        # get the importance sampling weight for the lowest priority experience; the lowest priority experience has the heighest weight (i.e. maximum weight) as the probability of picking it is so low
        max_weight = (p_min * self.batch_size) ** (-self.PER_b)

        for i in range(self.batch_size):
            # pick a random value from this priority segment
            lower, upper = priority_segment * i, priority_segment * (i+1)
            value = np.random.uniform(lower, upper)

            # get the leave for this random value
            index, priority, experience = self.get_leaf(value)

            # calculate the probability of this leaf and thus how important it should be
            sampling_probabilities = priority/ self.total_priority
            experience_ISWeights[i, 0] = np.power(self.batch_size * sampling_probabilities, -self.PER_b) / max_weight

            # remember the leaf index
            experience_idx[i] = index

            # grab the actual experience
            mini_batch.append(experience)

        return experience_idx, mini_batch, experience_ISWeights

    def sample(self, device):
        """Randomly sample a batch of experiences from memory"""
        tree_idx, experiences, weights  = self.make_batch()

        weights = torch.from_numpy(weights).float().to(device)

        states = torch.from_numpy(np.vstack([e.state for e in experiences if e is not None])).float().to(device)
        actions = torch.from_numpy(np.vstack([e.action for e in experiences if e is not None])).long().to(device)
        rewards = torch.from_numpy(np.vstack([e.reward for e in experiences if e is not None])).float().to(device)
        next_states = [e.next_state for e in experiences if e is not None]
        dones = torch.from_numpy(np.vstack([e.done for e in experiences if e is not None]).astype(np.uint8)).float().to(
            device)
        global_rewards = torch.from_numpy(
            np.vstack([e.global_reward for e in experiences if e is not None])).float().to(device)

        return (tree_idx, [states, actions, rewards, next_states, dones, global_rewards], weights)

    def __len__(self):
        """
        :return: keeps track of the number of items in the tree.
        """
        return self.num_data_points

    @property
    def total_priority(self):
        return self.tree[0]

class ReplayBuffer:
    """
    Fixed -size buffe to store experience tuples.
    Its used by the agent to collect actions, rewards etc to learn upon.
    """

    def __init__(self, buffer_size, batch_size):
        """Initialize a ReplayBuffer object.

        Params
        ======
            action_size (int): dimension of each action
            buffer_size (int): maximum size of buffer
            batch_size (int): size of each training batch
            seed (int): random seed
        """

        self.memory = deque(maxlen=buffer_size)
        self.batch_size = batch_size
        self.experiences = namedtuple("Experience", field_names=["state",
                                                                 "action",
                                                                 "reward",
                                                                 "next_state",
                                                                 "done",
                                                                 "global_reward"])

    #         self.seed = random.seed(seed)

    def add(self, state, action, reward, next_state, done, global_reward, predicted_reward):
    # def add(self, state, action, reward, next_state, done, global_reward):
        """Add a new experience to memory."""
        e = self.experiences(state, action, reward, next_state, done, global_reward)
        self.memory.append(e)

    def sample(self, device):
        """Randomly sample a batch of experiences from memory"""
        experiences = random.sample(self.memory, k=self.batch_size)

        states = torch.from_numpy(np.vstack([e.state for e in experiences if e is not None])).float().to(device)
        actions = torch.from_numpy(np.vstack([e.action for e in experiences if e is not None])).long().to(device)
        rewards = torch.from_numpy(np.vstack([e.reward for e in experiences if e is not None])).float().to(device)
        next_states = [e.next_state for e in experiences if e is not None]
        dones = torch.from_numpy(np.vstack([e.done for e in experiences if e is not None]).astype(np.uint8)).float().to(
            device)
        global_rewards = torch.from_numpy(
            np.vstack([e.global_reward for e in experiences if e is not None])).float().to(device)

        return (states, actions, rewards, next_states, dones, global_rewards)

    def __len__(self):
        """Return the current size of internal memory."""
        return len(self.memory)