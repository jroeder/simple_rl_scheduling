from DAG import DAG
from Schedule import Schedule
from Architecture import Architecture

class Environment():
    """
    One environment for one taskgraph.
    The main components are the taskgraph, schedule and the architecture of the platform.
    It provides glue code for getting the possible actions (tasks that can be scheduled),
    setting and getting features, advancing the environment (i.e. adding a task to the schedule)
    and resetting the environment for the next epoch.
    """

    def __init__(self, text_array, task_graph_num, num_cores):
        self.task_graph = DAG(text_array, task_graph_num)
        self.schedule = Schedule()
        self.architecture = Architecture(num_cores)

    def get_best_start_time(self, task, task_runtime, core):
        """
        :param task: the task we want the start time for
        :param core: the core on which the task is supposed to be scheduled
        :return: returns the best start time of a task
        """
        # min_start_time = self.task_graph.get_end_maximum_predecessor(task)
        # core_avail = self.architecture.get_cores_availability()[core]
        # best_start = max(min_start_time, core_avail)
        # return best_start

        # trying fls gap filling approach
        min_start_time = self.task_graph.get_end_maximum_predecessor(task)
        min_start_time = self.architecture.get_task_best_time(min_start_time, task_runtime, core)
        return min_start_time

    def set_static_task_features(self, features, struct2vec_path= ""):
        """The features are the last part in the tgff file and contain the """
        self.task_graph.set_static_task_features(features, struct2vec_path)

    def get_list_actions(self):
        return self.task_graph.schedulable_tasks()

    def get_list_of_features(self, task):
        """
        Collects all features for a task in an environment (from architecture, taskgraph and schedule)
        """
        core_states = self.architecture.get_cores_availability()
        task_features = list(core_states.values())
        task_features.extend(self.architecture.get_core_utilisation(self.schedule.get_makespan()))
        # task_features = list(self.architecture.get_core_utilisation(self.schedule.get_makespan()))
        task_features.extend(self.architecture.get_max_gap_all_cores())
        # task_features.extend(self.architecture.get_cluster_utilisation()) #TODO at some point
        task_features.extend(self.task_graph.get_task_features(task))
        task_features.extend(self.task_graph.get_global_features())
        task_features.extend(self.schedule.get_schedule_features())
        return task_features

    def step(self, action):
        """
        :param action: best task and its action; adds it to the schedule
        :return: reward and possible next tasks to schedule
        """
        target_core = "core" + str(action[1])
        best_task = action[0]

        # update tg, schedule and architecture, start_time, task_runtime, target_core
        task_runtime = float(self.task_graph.task_runtime[best_task])
        best_start = self.get_best_start_time(best_task, task_runtime, target_core)

        self.task_graph.add_task_scheduled(best_task, best_start, target_core)
        reward = self.schedule.add_task(best_task, best_start, task_runtime, target_core)
        self.architecture.increase_core_time(target_core, best_start, task_runtime)

        # compute reward and update network
        states = {}
        possible_tasks = self.get_list_actions()
        if not possible_tasks:
            return reward, {}

        for pt in possible_tasks:
            states[pt] = self.get_list_of_features(pt)
        return reward, states

    def reset(self):
        self.task_graph.reset_graph()
        self.schedule.reset_schedule()
        self.architecture.init_cores()