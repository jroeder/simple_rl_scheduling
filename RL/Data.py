from Environment import Environment
from torch.utils.data import Dataset
import os

class Data(Dataset):
    """
    A dictionary/set of environments (task graphs) plus some glue code to make the dataset play well with pytorch.
    Plus a long function to parse the tgff input file and the number of cores.
    """

    def __init__(self, file, num_cores, struct2vec_features_path):
        self.task_graphs = {}
        self.struct2vec_features_path = struct2vec_features_path
        self.init_dags_file(file, num_cores)

    def __len__(self):
        return len(self.task_graphs)

    def __getitem__(self, idx):
        return self.task_graphs[idx]

    def add_task_scheduled(self, idx, task):
        pass

    def set_opt_graph(self, directory):
        dags = os.listdir(directory)

        for dag in dags:
            dag_dir_path = os.path.join(directory, dag)
            dag_path = os.path.join(dag_dir_path, "heuristic.txt")

            if (os.path.isdir(dag_dir_path)):
                dag_num = int(dag[3:])
                if dag_num in self.task_graphs:
                    self.task_graphs[dag_num].task_graph.set_optimum(dag_path)

    def init_dags_file(self, file, num_cores):
        text_array = []
        graph_start = False
        graph_end = False
        feature_start = False
        features = []

        with open(file, "r") as dag_file:
            for line in dag_file:
                if "TASK_GRAPH" in line:
                    task_graph_num = int(line.split()[1])
                    graph_start = True
                    graph_end = False
                if "HARD_DEADLINE" in line:
                    graph_start = False
                    graph_end = True

                if graph_start:
                    text_array.append(line)

                if graph_end:
                    self.task_graphs[task_graph_num] = Environment(text_array, task_graph_num, num_cores)
                    text_array = []
                    graph_end = False
                    graph_start = False
                #             break

                if "}" in line:
                    feature_start = False
                elif feature_start:
                    features.append(line)
                elif "type version exec_time" in line:
                    feature_start = True

        for dag in self.task_graphs:
            struct2vec_features_path = os.path.join(self.struct2vec_features_path, "DAG" + str(dag), "DAG" + str(dag) + ".features")
            self.task_graphs[dag].set_static_task_features(features, struct2vec_features_path)