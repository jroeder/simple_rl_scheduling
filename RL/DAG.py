from utils import get_num
import numpy as np

class DAG():
    """
    Contains the taskgraph information.
    Including the scheduled tasks and all task and global features connected to the taskgraph.
    """

    def __init__(self, text_list=None, ID=None):
        self.ID = ""
        self.tasks = {}  # task name = key, item = task Type
        self.edges = []
        self.successors = {}
        self.predecessors = {}
        self.task_runtime = {}

        self.task_features = {}  # TODO: make into dict of dicts?
        self.global_features = []
        self.struct2vec_features = {}
        self.tasks_scheduled = {} # task name = key, {start time, target core}
        self.ILP_makespan = 0

        if text_list:
            self.init_dag_from_list(text_list, ID)
            self.make_successors_predecessors()

    def init_dag_from_list(self, text_list, ID):
        self.ID = ID
        for line in text_list:
            split_line = line.split()
            if "TASK " in line:
                self.tasks[split_line[1]] = split_line[3]
                self.successors[split_line[1]] = []
                self.predecessors[split_line[1]] = []
                self.task_features[split_line[1]] = []
            elif "ARC" in line:
                self.edges.append([split_line[3], split_line[5]])

    def make_successors_predecessors(self):
        for task in self.tasks.keys():
            for edge in self.edges:
                if task == edge[0]:
                    self.successors[task].append(edge[1])
                elif task == edge[1]:
                    self.predecessors[task].append(edge[0])

    def get_successors(self, task):
        return self.successors[task]

    def get_predecessors(self, task):
        return self.predecessors[task]

    def get_task_start_time(self, task):
        if task in self.tasks_scheduled:
            return self.tasks_scheduled[task]["start_time"]
        else:
            raise Exception("Task not yet scheduled: ", task)

    def get_end_maximum_predecessor(self, task):
        """
        Gets the maximum end time of all predecessors.
        """
        max_pred_start = 0

        for pred in self.get_predecessors(task):
            temp_max = self.get_task_start_time(pred) + self.task_runtime[pred]
            #             print(temp_max)
            if temp_max > max_pred_start:
                max_pred_start = temp_max

        return max_pred_start

    def get_target_core_max_predecessors(self, task):
        """
        Gets where the maximum end time predecessor  is scheduled.
        """
        max_pred_start = 0
        max_pred = 0

        for pred in self.get_predecessors(task):
            temp_max = self.get_task_start_time(pred) + self.task_runtime[pred]
            if temp_max > max_pred_start:
                max_pred_start = temp_max
                max_pred = pred

        if (not max_pred):
            return 0
        else:
            return int(get_num(self.tasks_scheduled[max_pred]["target_core"])[0][0])

    def get_task_features(self, task):
        dyn_features = []
        dyn_features.append( self.get_end_maximum_predecessor(task))
        dyn_features.append( self.get_target_core_max_predecessors(task))
        return_list = [*self.task_features[task], *self.struct2vec_features[task]]
        # return (self.task_features[task] + dyn_features)
        return (return_list + dyn_features)

    def add_task_scheduled(self, task, time, core):
        """
        time = start time
        """
        if task in self.tasks:
            for pred in self.get_predecessors(task):
                if pred not in self.tasks_scheduled:
                    raise Exception("Predecessors not scheduled")
            self.tasks_scheduled[task] = {"start_time": time, "target_core": core}
        else:
            raise Exception("Not a valid task")

    def schedulable_tasks(self):
        schedulable_tasks = []

        temp_predecessors = {}
        for task in self.predecessors:
            temp_predecessors = self.predecessors[task].copy()
            for task_scheduled in self.tasks_scheduled.keys():
                temp_predecessors = [x for x in temp_predecessors if x != task_scheduled]
            if not temp_predecessors:
                schedulable_tasks.append(task)

        for task in self.tasks_scheduled.keys():
            schedulable_tasks = [x for x in schedulable_tasks if x != task]

        return schedulable_tasks

    def set_static_task_features(self, feature_set, struct2vec_path = ""):
        """
        Sets task specific features.
        Runtime of task
        number of direct successors
        number of direct predecessors
        """
        # TODO: longest path until end.
        # TODO: longest path runtime until end

        # TODO: shortest path until end.
        # TODO: shortest path runtime until end

        # TODO: max runtime of direct successors
        # TODO: min runtime of direct successors
        # TODO: average runtime of direct successors

        # TODO: max time of the direct predecessors
        # TODO: min time of the direct predecessors
        # TODO: average time of the direct predecessors

        # sets the runtime for the task;
        for line in feature_set:
            split_line = line.split()

            for task in self.tasks:
                if self.tasks[task] == split_line[0]:
                    self.task_runtime[task] = float(split_line[2])
                    self.task_features[task].append(float(split_line[2]))

        for task in self.tasks:
            #           num direct successors
            self.task_features[task].append(len(self.successors[task]))
            #           num direct predecessors
            self.task_features[task].append(len(self.predecessors[task]))

        if struct2vec_path:
            features = np.loadtxt(struct2vec_path, skiprows=1, dtype=np.float32)

            for task in features:
                self.struct2vec_features["t"+str(int(self.ID))+"_"+str(int(task[0]))] = task[1:]

    def get_statistics_readyq(self):
        max_runtime = 0
        min_runtime = float("inf")
        average = []

        for task in self.schedulable_tasks():
            temp_rtime = self.task_runtime[task]
            max_runtime = max(max_runtime, temp_rtime)
            min_runtime = max(min_runtime, temp_rtime)
            average.append(temp_rtime)

        if (min_runtime == float("inf")):
            min_runtime = 0

        if (not average):
            average.append(0)

        return float(max_runtime), float(min_runtime), float(np.mean(average))

    def get_statistics_doneq(self):
        max_runtime = 0
        min_runtime = float("inf")
        average = []

        for task in self.tasks_scheduled.keys():
            temp_rtime = self.task_runtime[task]
            max_runtime = max(max_runtime, temp_rtime)
            min_runtime = max(min_runtime, temp_rtime)
            average.append(temp_rtime)

        if (min_runtime == float("inf")):
            min_runtime = 0

        if (not average):
            average.append(0)


        return float(max_runtime), float(min_runtime), float(np.mean(average))

    def get_global_features(self):
        """
        Gets the current "global" features, i.e. features that are the same for all tasks.
        max, min and average in readyq
        max, min and average in doneq
        num tasks in DAG
        Number of tasks available for scheduling
        Number of tasks that stilll need to be scheduled

        """

        self.global_features = []

        # max, min and average in readyq
        max_runtime, min_runtime, average = self.get_statistics_readyq()
        self.global_features.append(max_runtime)
        self.global_features.append(min_runtime)
        self.global_features.append(average)

        # max, min and average in doneq
        max_runtime, min_runtime, average = self.get_statistics_doneq()
        self.global_features.append(max_runtime)
        self.global_features.append(min_runtime)
        self.global_features.append(average)

        # number of tasks in DAG
        self.global_features.append(len(self.tasks))

        # num of currently avail for scheduling
        self.global_features.append(len(self.schedulable_tasks()))
        # num tasks still need scheduling
        self.global_features.append(len(self.tasks) - len(self.tasks_scheduled))

        return (self.global_features)

    def set_optimum(self, opt_file):

        with open(opt_file, "r") as f:
            for line in f:
                if "Final Schedule length" in line:
                    # divided by 10 for deca seconds to seconds (ds)
                    # TODO: make dependent on unit in line
                    self.ILP_makespan = int(get_num(line)[0][2]) / 10

    def reset_graph(self):
        self.tasks_scheduled = {}
        self.global_features = []