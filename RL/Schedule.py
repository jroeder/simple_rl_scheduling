from PIL import Image, ImageDraw
from matplotlib.pyplot import imshow
import numpy as np


class Schedule():
    """
    The current schedule.
    Includes functions to calculate the makespan adding a task to the schedule etc.
    """

    def __init__(self, num_cores=4):
        self.schedule = {}
        self.makespan = 0
        self.num_cores = num_cores
        self.schedule_features = []

    def calculate_makespan(self):
        temp_max = 0
        for task in self.schedule:
            temp_max = max(temp_max, self.schedule[task][1])
        self.makespan = temp_max

    def get_makespan(self):
        return self.makespan

    def get_schedule_features(self):
        """Features related to the schedule"""
        self.schedule_features = []

        self.schedule_features.append(self.get_makespan())
        return (self.schedule_features)

    #     def add_task(self, task, start_time, task_runtime, target_core):
    #         # returns local reward
    #         self.schedule[task] = [start_time, start_time+task_runtime, target_core]
    #         old_makespan = self.makespan
    #         self.calculate_makespan()
    #         if self.makespan > old_makespan:
    #             return 1/(self.makespan - old_makespan)
    #         else:
    #             return self.makespan

    def add_task(self, task, start_time, task_runtime, target_core):
        # returns local reward
        self.schedule[task] = [start_time, start_time + task_runtime, target_core]
        old_makespan = self.makespan
        self.calculate_makespan()
        return (old_makespan-self.makespan)

    def calculate_global_reward(self):
        self.calculate_makespan()
        return -self.makespan

    def reset_schedule(self):
        self.schedule = {}
        self.makespan = 0

    def draw_schedule(self, epoch):
        """
        visualizes the schedule
        """
        self.num_cores = 4
        height_image = self.num_cores * 100
        length_image = int(self.makespan + 10)

        core_coordinates = {}
        for i in range(1, self.num_cores + 1):
            core_coordinates["core" + str(i - 1)] = [i * 75, i * 75 + 50]

        im = Image.new('RGB', (int(length_image), int(height_image)), (128, 128, 128))
        draw = ImageDraw.Draw(im)

        for task in self.schedule.items():
            draw.rectangle((task[1][0], core_coordinates[task[1][2]][0], task[1][1], core_coordinates[task[1][2]][1]),
                           fill=(0, 192, 192), outline=(255, 255, 255))

        half = 3
        out = im.resize([int(half * s) for s in im.size])
        out.save("../img_out/" + str(epoch) +"_image.jpg")


        # imshow(np.asarray(out))
