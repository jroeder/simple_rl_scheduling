import torch.nn as nn
import torch.nn.functional as F
import torch

class QNetwork(nn.Module):
    """
    The actual neural network used by the agent to learn how to interact with the environment.
    """

    def __init__(self, state_size, action_size, dropout, fc1_unit=64, fc2_unit=64, fc3_unit=64, fc4_unit=64, fc5_unit = 64, fc6_unit = 64, fc7_unit = 64, fc8_unit = 64):
        """
        Initialize parameters and build model.
        Params
        =======
            state_size (int): Dimension of each state
            action_size (int): Dimension of each action
            seed (int): Random seed
            fc1_unit (int): Number of nodes in first hidden layer
            fc2_unit (int): Number of nodes in second hidden layer
        """

        num_value = 64
        num_actions = 64

        super(QNetwork, self).__init__()  ## calls __init__ method of nn.Module class
        self.fc1 = self._fc_layer(state_size, fc1_unit, dropout)
        self.fc2 = self._fc_layer(fc1_unit, fc2_unit, dropout)
        self.fc3 = self._fc_layer(fc2_unit, fc3_unit, dropout)
        self.fc4 = self._fc_layer(fc3_unit, fc4_unit, dropout)
        self.fc5 = self._fc_layer(fc4_unit, fc5_unit, dropout)
        self.fc6 = self._fc_layer(fc5_unit, fc6_unit, dropout)
        # self.fc7 = self._fc_layer(fc6_unit, fc7_unit, dropout)
        # self.fc8 = self._fc_layer(fc7_unit, fc8_unit, dropout)

        # Not splitting the layers
        self.f_out = nn.Linear(fc6_unit, action_size)

        # Splitting the network into 2. One part predicting the values. and one predicting the actions.

        # First option just a linear layer with relu and dropout
        # self.value = self._fc_layer(fc4_unit,1, dropout)
        # self.actions = self._fc_layer(fc4_unit, action_size, dropout)

        # Second option just a linear layer
        # self.value = nn.Linear(fc4_unit,1)
        # self.actions = nn.Linear(fc4_unit, action_size)

        # Third Option first layer and relu then linear layer
        # self.value_fc = self._fc_layer(fc4_unit, num_value, dropout)
        # self.value = nn.Linear(num_value, 1)
        #
        # self.actions_fc = self._fc_layer(fc4_unit, num_actions, dropout)
        # self.actions = nn.Linear(num_actions, action_size)

    def _fc_layer(self, input_size, output_size, dropout):
        return nn.Sequential(
            nn.Linear(input_size, output_size),
            nn.Dropout(dropout),
            nn.ReLU()
        )

    def forward(self, x):
        # x = state
        """
        Build a network that maps state -> action values.
        """
        x = self.fc1(x)
        x = self.fc2(x)
        x = self.fc3(x)
        x = self.fc4(x)
        x = self.fc5(x)
        x = self.fc6(x)
        # x = self.fc7(x)
        # x = self.fc8(x)

        # Dont Split network
        output = self.f_out(x)

        # Splitting the network into 2. One part predicting the values. and one predicting the actions.
        # just one layer per split network
        # state_value = self.value(x)
        # actions = self.actions(x)

        # two layers per split
        # state_value = self.value_fc(x)
        # state_value = self.value(state_value)
        #
        # actions = self.actions_fc(x)
        # actions = self.actions(actions)
        #
        # # Agregating layer
        # # Q(s,a) = V(s) + (A(s,a) - 1/|A| * sum A(s,a'))
        # output = state_value + torch.sub(actions, torch.mean(actions, dim=1, keepdim=True))

        return output