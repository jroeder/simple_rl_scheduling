import yaml
import argparse
import sys

"""
Author: Helena Russello (helena@russello.dev)
Edited: Julius Roeder (j.roeder@uva.nl)
"""

class MyConfig(object):

    def __init__(self, args):
        """
         A custom class for reading and parsing a YAML configuration file.

        :param config_path: the path of the configuration file
        """
        config_path = args.config

        self.config = None
        with open(config_path) as f:
            self.config = yaml.load(f, Loader=yaml.SafeLoader)

        if self.config is not None:
            # Mandatory parameters
            self.training_data = self.config['training_data']
            self.test_data = self.config['test_data']
            self.save_checkpoint = self.config['save_checkpoint']
            self.load_checkpoint = self.config['load_checkpoint']

            self.logging_frequency = self.config['logging_frequency']
            self.lr = self.config['lr']
            self.epochs = self.config['epochs']
            self.batch_size = self.config['batch_size']
            self.buffer_size = self.config['buffer_size']
            self.min_replay_memory_size = self.config['min_replay_memory_size']
            self.gamma = self.config['gamma']
            self.tau = self.config['tau']
            self.update_target_frequency = self.config['update_target_frequency']
            self.eps_decay = self.config['eps_decay']
            self.start_eps = self.config['start_eps']
            self.device = self.config['device']
            self.dropout = self.config['dropout']
            self.optimizer = self.config['optimizer']
            self.loss = self.config['loss']
            self.training_data_struct2vec_path = self.config['training_data_struct2vec_path']
            self.test_data_struct2vec_path = self.config['test_data_struct2vec_path']
            self.PRB = self.config['PrioritizedReplayBuffer']

            # Optional Parameters
            if 'test_data_opt' in self.config:
                self.test_data_opt = self.config['test_data_opt']

            if 'seed' in self.config:
                self.seed = self.config['seed']

            if 'regularize' in self.config:
                self.mean = self.config['regularize']['mean']
                self.std = self.config['regularize']['std']
                self.regularize = self.config['regularize']

            if 'lr_decay' in self.config:
                self.lr_decay = self.config['lr_decay']

            if 'group' in self.config:
                self.group = self.config['group']
            else:
                self.group = ""

            if 'optimizer' in self.config:
                self.optimizer = self.config['optimizer']
            else:
                self.optimizer = 'default'

        # override parameters in config file if specified in arguments
        if args.logging_frequency:
            self.logging_frequency = args.logging_frequency
            self.config['logging_frequency'] = args.logging_frequency
        if args.lr:
            self.lr = args.lr
            self.config['lr'] = args.lr
        if args.epochs:
            self.epochs = args.epochs
            self.config['epochs'] = args.epochs
        if args.batch_size:
            self.batch_size = args.batch_size
            self.config['batch_size'] = args.batch_size
        if args.optimizer:
            self.optimizer = args.optimizer
            self.config['optimizer'] = args.optimizer
        if args.group:
            self.group = args.group
            self.config['group'] = args.group
        if args.seed:
            self.seed = args.seed
            self.config['seed'] = args.seed
        if args.gamma:
            self.gamma = args.gamma
            self.config['gamma'] = args.gamma
        if args.tau:
            self.tau = args.tau
            self.config['tau'] = args.tau
        if args.update_target_frequency:
            self.update_target_frequency = args.update_target_frequency
            self.config['update_target_frequency'] = args.update_target_frequency
        if args.optimizer:
            self.optimizer = args.optimizer
            self.config['optimizer'] = args.optimizer
        if args.loss:
            self.loss = args.loss
            self.config['loss'] = args.loss
        if args.eps_decay:
            self.eps_decay = args.eps_decay
            self.config['eps_decay'] = args.eps_decay
        if args.loss:
            self.start_eps = args.start_eps
            self.config['start_eps'] = args.start_eps
        if args.buffer_size:
            self.buffer_size = args.buffer_size
            self.config['buffer_size'] = args.buffer_size
            if self.PRB:
                self.min_replay_memory_size = args.buffer_size
                self.config['min_replay_memory_size'] = args.buffer_size

    def __str__(self):
        return str(self.config)


def parse_args(description):
    """
    Parse arguments and process the configuration file
    :return: the config and the arguments
    """
    parser = argparse.ArgumentParser(description=description)
    # config file
    parser.add_argument('--config',
                        help='YAML configuration file',
                        default="config_Adam.yaml",
                        type=str)
    # training hyper parameters to be overriden from the config file
    parser.add_argument('--logging_frequency',
                        help='frequency of logging',
                        type=int)
    parser.add_argument('--lr',
                        help='Learning rate',
                        type=float)
    parser.add_argument('--epochs',
                        help='Number of epochs',
                        type=int)
    parser.add_argument('--batch_size',
                        help='Batch size',
                        type=int)
    parser.add_argument('--seed',
                        help='Random seed',
                        type=int)
    parser.add_argument('--group',
                        help='group name in wandb',
                        type=str)
    parser.add_argument('--gamma',
                        help='discount factor of the future rewards (i.e. higher = more important)',
                        type=float)
    parser.add_argument('--tau',
                        help='soft update of target parameters',
                        type=float)
    parser.add_argument('--update_target_frequency',
                        help='soft update of target parameters',
                        type=int)
    parser.add_argument('--optimizer',
                        help='optimizer to use',
                        type=str)
    parser.add_argument('--loss',
                        help='loss function to use',
                        type=str)
    parser.add_argument('--start_eps',
                        help='starting epsilon',
                        type=float)
    parser.add_argument('--eps_decay',
                        help='epsilon decay',
                        type=float)
    parser.add_argument('--buffer_size',
                        help='buffer size',
                        type=int)
    parser.add_argument('--dropout',
                        help='dropout',
                        type=float)

    args, rest = parser.parse_known_args()

    cfg = MyConfig(args)
    print(cfg)
    args = parser.parse_args()
    print(args)

    return cfg, args
