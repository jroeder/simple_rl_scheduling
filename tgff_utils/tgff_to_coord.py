import os
import shutil


class DAG():
    """
    Contains the taskgraph information.
    Including the scheduled tasks and all task and global features connected to the taskgraph.
    """
    def __init__(self, text_list=None, ID=None):
        self.ID = ""
        self.tasks = {}
        self.edges = []
        self.successors = {}
        self.predecessors = {}
        self.task_input_letter = {}
        self.task_output_letter = {}

        self.task_features = {}
        self.global_features = []
        self.tasks_scheduled = []
        self.ILP_makespan = 0

        if text_list:
            self.init_dag_from_list(text_list, ID)
            self.make_successors_predecessors()

    def init_dag_from_list(self, text_list, ID):
        self.ID = ID
        for line in text_list:
            split_line = line.split()
            if "TASK " in line:
                self.tasks[split_line[1]] = split_line[3]
                self.task_input_letter[split_line[1]] = "a"
                self.task_output_letter[split_line[1]] = "a"
                self.successors[split_line[1]] = []
                self.predecessors[split_line[1]] = []
                self.task_features[split_line[1]] = []
            elif "ARC" in line:
                self.edges.append([split_line[3], split_line[5]])

    def make_successors_predecessors(self):
        for task in self.tasks:
            for edge in self.edges:
                if task == edge[0]:
                    self.successors[task].append(edge[1])
                elif task == edge[1]:
                    self.predecessors[task].append(edge[0])

    def get_successors(self, task):
        return self.successors[task]

    def get_predecessors(self, task):
        return self.predecessors[task]

    def get_task_features(self, task):
        return self.task_features[task]

    def set_task_features(self, feature_set):
        """
        Runtime of task
        """
        # sets the runtime for the task;
        for line in feature_set:
            split_line = line.split()

            for task in self.tasks:
                if self.tasks[task] == split_line[0]:
                    self.task_features[task].append(float(split_line[2]))


class tgff_Converter():
    """
    converts tgff to coord graphs including the timing for the tasks
    """
    def __init__(self, target_folder):
        self.target_folder = target_folder
        self.list_DAGS = []

    def create_folder(self, task_graph_num):
        target_dir = os.path.join(self.target_folder, "DAG" + str(task_graph_num))
        os.makedirs(target_dir, exist_ok=True)

    def create_coords(self, dag):
        self.create_folder(dag.ID)

        target_file = os.path.join(self.target_folder, "DAG" + str(dag.ID), "DAG" + str(dag.ID) + ".coord")
        coord_file = ["app DAG" + str(dag.ID) +" {\n" , "deadline 100000s\n", "period 1Hz\n", "energy-available 100000J\n", "datatypes {\n", '(int_t, "int")\n', "}\n", "components {\n"]

        #make components
        for task in dag.tasks:
            coord_file.append(task + " {\n" )
            #inputs for each component
            if ( len(dag.predecessors[task]) > 0):
                coord_file.append("inputs [")
                letter = "a"
                for i, pred in enumerate(dag.predecessors[task]):
                    coord_file.append("(in_" + letter + ",1 , int_t)")
                    if (i+1) == len(dag.predecessors[task]):
                        coord_file.append("]\n")
                    else:
                        coord_file.append("\n")

                    letter = chr(ord(letter) + 1)
            #outputs for each component
            if ( len(dag.successors[task]) > 0):
                coord_file.append("outputs [")
                letter = "a"
                for i, pred in enumerate(dag.successors[task]):
                    coord_file.append("(out_" + letter + ",1 , int_t)")
                    if (i+1) == len(dag.successors[task]):
                        coord_file.append("]\n")
                    else:
                        coord_file.append("\n")
                    letter = chr(ord(letter) + 1)
            coord_file.append("}\n")
        coord_file.append("}\n")

        # make edges
        coord_file.append("edges { ")
        for edge in dag.edges:
            letter = "a"
            for i, suc in enumerate(dag.successors[task]):
                out_letter = dag.task_output_letter[edge[0]]
                in_letter = dag.task_input_letter[edge[1]]
                coord_file.append(edge[0] + ".out_" + out_letter + "->" + edge[1] + ".in_" + in_letter + "\n")
                dag.task_output_letter[edge[0]] = chr(ord(out_letter) + 1)
                dag.task_input_letter[edge[1]] = chr(ord(in_letter) + 1)

        coord_file.append("}\n") #for edges
        coord_file.append("}\n") #for end of coord


        with open(target_file, "w") as f:
            f.writelines(coord_file)

    def make_dag(self, text_array, task_graph_num):
        self.list_DAGS.append(DAG(text_array, task_graph_num))

    def create_nfp(self, dag):
        """
        Creates the nfp file for each graph
        :return:
        """
        nfp_file = ["Component Name,Component Version,Variable Name,Line Number,Filename,WCET,WCEngT,AET,AEngT,Security Level,Architecture,Binary,Callable Name,Callable,Confidence,Frequency,GPU_frequency\n\n"]
        target_file = os.path.join(self.target_folder, "DAG" + str(dag.ID), "non_func_prop.nfp")

        for task in dag.tasks:
            line = ""
            line += task + "," + task + "_1," #component name and version
            line += ",,," #Variable Name,Line Number,Filename
            line += str(dag.task_features[task][0]) + "s," # WCET
            line += ",,," #WCEngT,AET,AEngT
            line += "[0..100],armv7little,," #Security Level,Architecture,Binary
            line += task + ",,high," #Callable Name,Callable,Confidence
            line += "100Hz,0Hz\n" #cpu frequency, gpu frequency
            nfp_file.append(line)

        with open(target_file, "w") as f:
            f.writelines(nfp_file)

    def create_config(self):
        """
        Creates the configuration file for the ILP and FLS? - just need one file IMO
        :return:
        """
        pass

    def init_dags_file(self, file):
        text_array = []
        graph_start = False
        graph_end = False
        feature_start = False
        features = []

        with open(file, "r") as dag_file:
            for line in dag_file:
                if "TASK_GRAPH" in line:
                    task_graph_num = int(line.split()[1])
                    graph_start = True
                    graph_end = False
                if "HARD_DEADLINE" in line:
                    graph_start = False
                    graph_end = True

                if graph_start:
                    text_array.append(line)

                if graph_end:
                    self.make_dag(text_array, task_graph_num)
                    text_array = []
                    graph_end = False
                    graph_start = False

                if "}" in line:
                    feature_start = False
                elif feature_start:
                    features.append(line)
                elif "type version exec_time" in line:
                    feature_start = True

        for dag in self.list_DAGS:
            dag.set_task_features(features)
            self.create_coords(dag)
            self.create_nfp(dag)


if __name__ == "__main__":
    conv = tgff_Converter("/home/julius/Projects/phd/gnn_tgff_data/test_ILP_4cores")
    conv.init_dags_file("/home/julius/Projects/phd/gnn_tgff_data/test.tgff")