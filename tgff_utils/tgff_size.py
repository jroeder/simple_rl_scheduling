import numpy as np

def get_task_stats(file):
    graph_start = False
    graph_end = False

    number_of_tasks = 0
    num_tasks = []

    with open(file, "r") as dag_file:
        for line in dag_file:
            if "TASK_GRAPH" in line:
                graph_start = True
                graph_end = False
            if "HARD_DEADLINE" in line:
                graph_start = False
                graph_end = True

            if graph_start:
                if "TASK" in line:
                    number_of_tasks += 1


            if graph_end:
                num_tasks.append(number_of_tasks)
                number_of_tasks = 0
                graph_end = False
                graph_start = False

    print(np.mean(num_tasks))
    print(np.min(num_tasks))
    print(np.max(num_tasks))
    print(np.std(num_tasks))


get_task_stats("/home/julius/Projects/phd/gnn_tgff_data/train.tgff")
get_task_stats("/home/julius/Projects/phd/gnn_tgff_data/test.tgff")
