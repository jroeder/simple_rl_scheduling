import numpy as np
import os

def analyse_schedule_time(path):
    folders = os.listdir(path)
    schedule_times = []

    for folder in folders:
        folder_path = os.path.join(path, folder)
        if "DAG" in folder and os.path.isdir(folder_path):
            file_path = os.path.join(folder_path, "heuristic.txt")
            with open(file_path, "r") as f:
                for l in f:
                    if "Schedule time" in l:
                        split_l = l.split(" ")
                        schedule_times.append(float(split_l[4]))

    print(np.mean(schedule_times))

if __name__ == "__main__":
    analyse_schedule_time("/home/julius/Projects/phd/gnn_tgff_data/test_output/")
