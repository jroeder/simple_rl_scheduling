import os
import subprocess

def computed_DAGS(directory, output_dir, ilp=False, heuristic=False, makespan=False, makespan_single=False):
    list_dags = os.listdir(directory)

    os.chdir(directory)

    # ilp_command = ["/home/julius/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/cmake-build-release/dist/methane", "--config", "REPLACE/configs/ILP/ilp.xml", "--coord", "REPLACE/REPLACE.coord", "--nfp", "REPLACE/non_func_prop.nfp"]
    # makespan_command = ["/home/julius/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/cmake-build-release/dist/methane", "--config", "REPLACE/configs/makespan/makespan.xml", "--coord", "REPLACE/REPLACE.coord", "--nfp", "REPLACE/non_func_prop.nfp"]
    # makespan_command_single = ["/home/jroeder/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/build/dist/methane", "--config", "REPLACE/configs/heuristics/heuristics.xml", "--coord", "REPLACE/REPLACE.coord", "--nfp", "REPLACE/non_func_prop_cpu.nfp"]
    heuristic_command = ["/home/julius/Projects/phd/TeamPlay_Tools/CoordinationTools/compiler/cmake-build-release/dist/methane", "--config", "heuristic.xml", "--coord", "REPLACE/REPLACE.coord", "--nfp", "REPLACE/non_func_prop.nfp"]

    for i, dag in enumerate(list_dags):
        dag_command = []
        if os.path.isdir(dag) and "DAG" in dag:
            original = os.path.join(directory, dag, os.path.basename(dag) + ".svg")

            if ilp:
                for part in ilp_command:
                    dag_command.append(part.replace("REPLACE", os.path.basename(dag)))
                out = os.path.join (output_dir,dag,"ilp.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_ilp.svg")
                    if not os.path.exists(os.path.join(output_dir, dag)):
                        os.makedirs(os.path.join(output_dir, dag))
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if makespan:
                dag_command = []
                for part in makespan_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"makespan.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_makespan.svg")
                    if not os.path.exists(os.path.join(output_dir, dag)):
                        os.makedirs(os.path.join(output_dir, dag))
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if makespan_single:
                dag_command = []
                for part in makespan_command_single:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"cpu_version_heuristic.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_cpu_version_heuristic.svg")
                    if not os.path.exists(os.path.join(output_dir, dag)):
                        os.makedirs(os.path.join(output_dir, dag))
                    try:
                        os.rename(original, new)
                    except:
                        pass

            if heuristic:
                dag_command = []
                for part in heuristic_command:
                    dag_command.append(part.replace("REPLACE", dag))
                out = os.path.join (output_dir,dag,"heuristic.txt")
                if not os.path.exists(os.path.join (output_dir,dag)):
                    os.makedirs(os.path.join (output_dir,dag))
                if not os.path.exists(out):
                    ilp_desc = open(out, 'w')
                    subprocess.call(dag_command, stdout=ilp_desc, stderr=subprocess.STDOUT)
                    ilp_desc.close()
                    new = os.path.join(output_dir, dag, os.path.basename(dag) + "_heuristic.svg")
                    try:
                        os.rename(original, new)
                    except:
                        pass


            print("DAG " + os.path.basename(dag) + " done")
